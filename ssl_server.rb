require 'socket'
require 'openssl'
require 'thread'

if ARGV.length < 3
  puts "Usage: ruby #{__FILE__} port ssl_cert_path ssl_key_path"
  exit 1
end

server  = TCPServer.new ARGV[0]
context = OpenSSL::SSL::SSLContext.new

context.cert = OpenSSL::X509::Certificate.new(File.open(ARGV[1]))
context.key  = OpenSSL::PKey::RSA.new(File.open(ARGV[2]), ARGV[3])

secure = OpenSSL::SSL::SSLServer.new(server, context)

puts "Listening securely on port #{ARGV[0]}..."

loop do
  client        = secure.accept                                                
  first_line    = client.gets                                                  
  verb, path, _ = first_line.split     
  template = "Hello {{user_id}}"                                        

  if verb == 'GET' && matched = path.match(/^\/customers\/(.*?)$/)             
    user_id  = matched[1]                                                      
    body     = template.gsub("{{user_id}}", user_id)                           
    response = "HTTP/1.1 200\r\nContent-Type: text/html\r\n\r\n#{body}"        

    client.puts(response)                                                      
  end                                                                          
end