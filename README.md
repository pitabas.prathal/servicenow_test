create ssl

```ruby create_ssl.rb```

Start SSL Server using:

```
ruby ssl_server.rb 9099 certificate.pem private.pem
```

Start SSL client using:

```
ruby ssl_client.rb 9099 certificate.pem
```