require 'socket'
require 'openssl'
require 'thread'

unless ARGV.length == 2
  puts "Usage: ruby #{__FILE__} port ssl_cert_file"
  exit 1
end

client  = TCPSocket.new '127.0.0.1', ARGV[0]
context = OpenSSL::SSL::SSLContext.new
context.verify_mode = OpenSSL::SSL::VERIFY_PEER | OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT
context.ca_file = ARGV[1]

secure = OpenSSL::SSL::SSLSocket.new(client, context)
secure.sync_close = true
secure.connect

Thread.new do
  begin
    while response = secure.gets
      $stdout.puts response
    end
  rescue
    $stderr.puts "Error from client: #{$!}"
  end
end

while request = $stdin.gets
  request = request.chomp
  secure.puts request
end