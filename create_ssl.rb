require './certificate_authority.rb'

ssl = CertificateAuthority.generate

certificate_file = File.new("certificate.pem", "w")
certificate_file.puts(ssl.cert.to_pem)
certificate_file.close

private_file = File.new("private.pem", "w")
private_file.puts(ssl.key.to_s)
private_file.close
